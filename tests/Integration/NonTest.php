<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusBundleTests\Integration;

final class NonTest extends FunctionalTestCase
{
    public function testNone(): void
    {
        $this->consume('memory');
        self::assertEquals(1, 1);
    }
}
