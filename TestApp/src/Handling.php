<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusBundleTestApp;

use GDXbsv\PServiceBus\Bus\Handling\Handle;
use JetBrains\PhpStorm\Immutable;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
#[Immutable]
class Handling
{
    #[Handle(transportName: 'memory1')]
    public function transport1(Message $message): void
    {
        return;
    }
    #[Handle(transportName: 'memory2')]
    public function transport2(Message $message): void
    {
        return;
    }
}
