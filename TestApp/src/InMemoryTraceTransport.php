<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBusBundleTestApp;

use GDXbsv\PServiceBus\Transport\Envelope;
use GDXbsv\PServiceBus\Transport\InMemoryTransport;

final class InMemoryTraceTransport extends InMemoryTransport
{
    public string $name = '';

    public array $envelopesRecorded = [];

    /**
     * @return \Generator<int, void, Envelope|null, void>
     * @psalm-suppress MixedReturnTypeCoercion Still do not know why
     */
    protected function sendingCoroutine(): \Generator
    {
        $parentSending = parent::sendingCoroutine();
        while (true) {
            $envelope = (yield);
            if (!$envelope) {
                $parentSending->send($envelope);
                break;
            }
            $parentSending->send($envelope);
            $this->envelopesRecorded[] = $envelope;
        }
    }
}
