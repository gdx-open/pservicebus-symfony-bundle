<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusBundle\DependencyInjection\Compiler;

use GDXbsv\PServiceBus\Bus\CoroutineBus;
use GDXbsv\PServiceBus\Transport\InMemoryTransport;
use GDXbsv\PServiceBus\Transport\TransportSyncConsoleCommand;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * @internal
 */
class TransportPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $syncs = [];
        foreach ($container->findTaggedServiceIds('p-service-bus.transport.sync') as $id => $tags) {
            $syncs[] = new Reference($id);
        }
        $transportSyncConsoleCommand = $container->getDefinition(TransportSyncConsoleCommand::class);
        $transportSyncConsoleCommand->setArgument('$transportSynchronisations', $syncs);
        foreach ($container->findTaggedServiceIds('transport.in_memory') as $id => $tags) {
            $definition = $container->getDefinition($id);
            $definition->addMethodCall('setBus', [new Reference(CoroutineBus::class)]);
        }
    }
}
