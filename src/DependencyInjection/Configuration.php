<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('p_service_bus');

        $rootNode = $treeBuilder->getRootNode();
        $rootNode
            ->children()
                ->arrayNode('transports')
                    ->isRequired()
                    ->useAttributeAsKey('name')
                    ->prototype('scalar')->end()
                ->end()
                ->arrayNode('doctrine')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->integerNode('pingTimeout')
                            ->min(0)
                            ->defaultValue(3600)
                            ->info('Set how often to check that connection is still open.')
                        ->end()
                        ->booleanNode('rollbackTransaction')
                            ->defaultValue(true)
                            ->info('Set to rollback transaction before each message.')
                        ->end()
                    ->end()
                ->end()
            ->end()
        ->end();

        return $treeBuilder;
    }
}
